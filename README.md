# GCP-Dataproc
## Description
This repository is for testing out PySpark on Yarn deployed using GCP Dataproc

## Goals
- Observe effects of `spark.dynamicAllocation.enabled: true`
- Observe effects of `persist` and its options (In Progress)

## Setup
### Go to DataProc
1. Specify Cluster Type to 1 Master (Since this is a poc)
![image.png](images/Cluster_Config.png)

2. Enable Components
![image.png](images/Components.png)


## Testing Setup
### Dataset
**Source:** Google Cloud Platform Big Query

**Table Id:** bigquery-public-data.covid19_open_data.covid19_open_data

### PySpark Script
```py
# These allow us to create a schema for our data
from pyspark.sql.types import StructField, StructType, StringType, LongType

# A Spark Session is how we interact with Spark SQL to create Dataframes
from pyspark.sql import SparkSession

# This will help catch some PySpark errors
from py4j.protocol import Py4JJavaError

# Create a SparkSession under the name "reddit". Viewable via the Spark UI
spark = SparkSession.builder.appName("covid19-open-data").getOrCreate()

# Create a two column schema consisting of a string and a long integer
fields = [StructField("location_key", StringType(), True),
          StructField("new_recovered", LongType(), True)]
schema = StructType(fields)

# Create an empty DataFrame. We will continuously union our output with this
recovered_counts = spark.createDataFrame([], schema)

# In the form of <project-id>.<dataset>.<table>
table = f"bigquery-public-data.covid19_open_data.covid19_open_data"
table_df = (spark.read.format('com.google.cloud.spark.bigquery').option('table', table).load())
recovered_counts = table_df.groupBy("location_key").count().sort("count", ascending=False).show()
```
### Submit Spark Job
1. Upload py script to Google Cloud Storage Bucket

2. Copy gsutil URI of py script from Google Cloud Storage Bucket
![image.png](images/Bucket_gsutilURI.png)

3. Go to Dataproc > Jobs > SUBMIT JOB

    `<gsutil URI>` in Main Python File

    `gs://spark-lib/bigquery/spark-bigquery-latest_2.12.jar` in Jar Files

    ![image.png](images/Submit_Job.png)

    Specify Additional Properties in *Properties*
    ![image.png](images/Submit_Job_Properties.png)

## Observations
### **Run 1:** Default DataProc Configuration
#### **Notable Configurations**
| Name | Value |
|------|-------|
| spark.dynamicAllocation.enabled | true |
| spark.dynamicAllocation.minExecutors | 1 |
| spark.executor.instances | 2 |
| spark.executor.memory | 5739m |

#### Spark Allocation
![image.png](images/Default_DataProc_Config.png)

### **Run 2:** spark.executor.memory 1g
#### **Configurations**
| Name | Value |
|------|-------|
| spark.executor.memory | 1g |

#### Spark Allocation
![image.png](images/DataProc_ExecutorMemory_1g.png)

### **Run 3:** Executor Memory 1g & dynamicAllocation Disabled
#### **Configurations**
| Name | Value |
|------|-------|
| spark.dynamicAllocation.enabled | true |
| spark.executor.instances | 2 |
| spark.executor.memory | 1g |

#### Spark Allocation
![image.png](images/DataProc_DynamicAllocation_Disabled.png)


### **Run 4:** Executor Memory 1g with Persist
#### **Configurations**
| Name | Value |
|------|-------|
| spark.dynamicAllocation.enabled | true |
| spark.dynamicAllocation.minExecutors | 1 |
| spark.executor.instances | 2 |
| spark.executor.memory | 1g |

#### Modifications to script
```py
# In the form of <project-id>.<dataset>.<table>
table = f"bigquery-public-data.covid19_open_data.covid19_open_data"
table_df = (spark.read.format('com.google.cloud.spark.bigquery').option('table', table).load())
filtered_df = table_df.filter("country_name == 'Malaysia'")
filtered_df.persist()   # This line is commented for "no persist" run
filtered_df.count()
recovered_counts = filtered_df.groupBy("location_key").count().sort("count", ascending=False)

recovered_counts.show()
```

#### Spark Allocation
![image.png](images/DataProc_Run4.png)


### **Run 5:** Executor Memory 1g without Persist
#### **Configurations**
| Name | Value |
|------|-------|
| spark.dynamicAllocation.enabled | true |
| spark.dynamicAllocation.minExecutors | 1 |
| spark.executor.instances | 2 |
| spark.executor.memory | 1g |

#### Modifications to script
```py
# In the form of <project-id>.<dataset>.<table>
table = f"bigquery-public-data.covid19_open_data.covid19_open_data"
table_df = (spark.read.format('com.google.cloud.spark.bigquery').option('table', table).load())
filtered_df = table_df.filter("country_name == 'Malaysia'")
# filtered_df.persist()   # This line is commented for "no persist" run
filtered_df.count()
recovered_counts = filtered_df.groupBy("location_key").count().sort("count", ascending=False)

recovered_counts.show()
```

#### Spark Allocation
![image.png](images/DataProc_Run5.png)



## Additional Information
- `scripts` folder contains py scripts. Including other scripts used for testing in the process of figuring out the tool.
- `local-data` folder contains `netflix movie.csv` used in `scripts/test.py`