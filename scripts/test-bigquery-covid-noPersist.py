# These allow us to create a schema for our data
from pyspark.sql.types import StructField, StructType, StringType, LongType

# A Spark Session is how we interact with Spark SQL to create Dataframes
from pyspark.sql import SparkSession

# This will help catch some PySpark errors
from py4j.protocol import Py4JJavaError

# Create a SparkSession under the name "reddit". Viewable via the Spark UI
spark = SparkSession.builder.appName("covid19-open-data").getOrCreate()

# Create a two column schema consisting of a string and a long integer
fields = [StructField("location_key", StringType(), True),
          StructField("new_recovered", LongType(), True)]
schema = StructType(fields)

# Create an empty DataFrame. We will continuously union our output with this
recovered_counts = spark.createDataFrame([], schema)

# In the form of <project-id>.<dataset>.<table>
table = f"bigquery-public-data.covid19_open_data.covid19_open_data"
table_df = (spark.read.format('com.google.cloud.spark.bigquery').option('table', table).load())
filtered_df = table_df.filter("country_name == 'Malaysia'")
# filtered_df.persist()
filtered_df.count()
recovered_counts = filtered_df.groupBy("location_key").count().sort("count", ascending=False)

recovered_counts.show()

