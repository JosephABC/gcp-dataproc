#! /usr/bin/python

import pyspark
from pyspark.sql import SparkSession

#Create List
numbers = [1,2,1,2,3,4,4,6]

#SparkContext
sc = pyspark.SparkContext()

# Creating RDD using parallelize method of SparkContext
rdd = sc.parallelize(numbers)

#Returning distinct elements from RDD
distinct_numbers = rdd.distinct().collect()

#Print
print('Distinct Numbers:', distinct_numbers)

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL basic example") \
    .getOrCreate()
df = spark.read.csv("gs://dataproc-staging-us-central1-167573616725-oays06lu/netflix-movie.csv")
df.show()

print(sc.getConf().getAll())